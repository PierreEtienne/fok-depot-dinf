package com.example.tennis

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.util.Log

//from:
//https://www.javatpoint.com/android-recyclerview-list-example

const val startingMoney = 500;

class MainActivity : AppCompatActivity() {
    private val TAG = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val sharedPreferences = getSharedPreferences("SharedPreference", MODE_PRIVATE)
        sharedPreferences.edit().clear().commit();
        val myEdit = sharedPreferences.edit()
        myEdit.putInt("money", startingMoney)
        myEdit.apply()

        createNotificationChannel()
        Log.i(TAG, "Channel Created")

        val recyclerView = findViewById<View>(R.id.recyclerView) as RecyclerView
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)

        var controller = Controller.CreateController(applicationContext)
        controller.getData(recyclerView)
        Thread(Runnable{
            refresh(controller, recyclerView)
        }).start()

        sendAction("init")
    }

    private fun sendAction(action: String?) {
        Intent(this, UpdaterService::class.java).also {
            it.action = action

            startService(it)
        }
    }

    private fun sendActionWithList(action: String, optional: ArrayList<MyListData>) {
        Intent(this, UpdaterService::class.java).also {
            it.action = action
            it.putExtra("dataList", optional)

            startService(it)
        }
    }

    fun refresh(controller: Controller, recyclerView: RecyclerView) {
        var firstRefresh = true
        while(true) {
            Log.i(TAG, "refresh")
            controller.getDataWithDelegate(recyclerView, Controller.MatchListDelegate {
                sendActionWithList("refreshWithData", it)
            })
            if (firstRefresh) firstRefresh = false
            else {
                sendAction("mainRefresh")
            }
            Thread.sleep(60_000)
        }
    }

    fun forceRefresh(view: android.view.View) {
        var recyclerView = findViewById<View>(R.id.recyclerView) as RecyclerView
        var controller = Controller.getInstance();
        controller.getData(recyclerView)
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val id = getString(R.string.channel_id)
            val name = getString(R.string.channel_name)
            val description = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(id, name, importance).apply {
                this.description = description
            }
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}