package com.example.tennis;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class Controller {

    private Context context;
    private ArrayList<MyListData> matchesArrayList;
    private static Controller controller;
    private static final String backend_url = "http://172.16.9.19:3000/";

    public static Controller CreateController(Context context) {
        controller = new Controller(context);
        return controller;
    }
    public static Controller getInstance() { return controller; }

    private Controller(Context context)
    {
        this.context = context;
    }

    public interface MatchListDelegate {
        void getListData(ArrayList<MyListData> matchesArrayList);
    }

    public void getData(RecyclerView recyclerView) {
        getDataWithDelegate(recyclerView, x -> {});
    }

    public void getDataWithDelegate(RecyclerView recyclerView, MatchListDelegate myDelegate) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String base_url = Controller.backend_url + "parties";
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, base_url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                matchesArrayList = new ArrayList<>();
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject responseObj = response.getJSONObject(i);

                        String tournament = responseObj.getString("tournoi");
                        String date = responseObj.getString("heure_debut");
                        String player1Name = responseObj.getJSONObject("joueur1").getString("prenom") + " " + responseObj.getJSONObject("joueur1").getString("nom");
                        String player1Rank = responseObj.getJSONObject("joueur1").getString("rang");
                        String player2Name = responseObj.getJSONObject("joueur2").getString("prenom") + " " + responseObj.getJSONObject("joueur2").getString("nom");
                        String player2Rank = responseObj.getJSONObject("joueur2").getString("rang");

                        String state = "À venir";
                        JSONArray sets = responseObj.getJSONObject("pointage").getJSONArray("jeu");
                        String etat = responseObj.getJSONObject("pointage").getString("etat");
                        if(etat.equals("en cours")){
                            state = "En cours";
                        }else if(etat.equals("terminé")){
                            state = "Terminé";
                        }
                        String player1Set1 = "";
                        String player2Set1 = "";
                        String player1Set2 = "";
                        String player2Set2 = "";
                        String player1Set3 = "";
                        String player2Set3 = "";
                        if(!sets.isNull(0) && !state.equals("À venir")){
                            player1Set1 = sets.getJSONArray(0).get(0).toString();
                            player2Set1 = sets.getJSONArray(0).get(1).toString();
                        }
                        if(!sets.isNull(1) ){
                            player1Set2 = sets.getJSONArray(1).get(0).toString();
                            player2Set2 = sets.getJSONArray(1).get(1).toString();
                        }
                        if(!sets.isNull(2) ){
                            player1Set3 = sets.getJSONArray(2).get(0).toString();
                            player2Set3 = sets.getJSONArray(2).get(1).toString();
                        }


                        matchesArrayList.add(new MyListData(state, tournament, date, player1Name, player1Rank, player2Rank, player2Name, player1Set1, player1Set2, player1Set3, player2Set1, player2Set2, player2Set3));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                myDelegate.getListData(matchesArrayList);
                MyListAdapter adapter = new MyListAdapter(matchesArrayList);
                recyclerView.setAdapter(adapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //recyclerView.setText(error.toString());
            }
        });

        queue.add(request);

    }

    public void getDataPartie(ResumeMatch Res, int position) {
        MyListResume resume;
        RequestQueue queue = Volley.newRequestQueue(context);
        String base_url = Controller.backend_url + "parties/" + position;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, base_url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject responseObj) {
                MyListResume resume = new MyListResume();
                    try {

                        String tournament = responseObj.getString("tournoi");
                        String date = responseObj.getString("heure_debut");
                        String terrain = responseObj.getString("terrain");
                        String tempPartie = responseObj.getString("temps_partie");
                        String player1Name = responseObj.getJSONObject("joueur1").getString("prenom") + " " + responseObj.getJSONObject("joueur1").getString("nom");
                        String player1Rank = responseObj.getJSONObject("joueur1").getString("rang");
                        String player1Age = responseObj.getJSONObject("joueur1").getString("age");
                        String player1Pays = responseObj.getJSONObject("joueur1").getString("pays");
                        String player2Name = responseObj.getJSONObject("joueur2").getString("prenom") + " " + responseObj.getJSONObject("joueur2").getString("nom");
                        String player2Rank = responseObj.getJSONObject("joueur2").getString("rang");
                        String player2Age = responseObj.getJSONObject("joueur2").getString("age");
                        String player2Pays = responseObj.getJSONObject("joueur2").getString("pays");
                        String serveur = responseObj.getString("serveur");
                        if (serveur.equals("0")){
                            serveur = player1Name;
                        } else {
                            serveur = player2Name;
                        }
                        String vitesse = responseObj.getString("vitesse_dernier_service");
                        String nbCoup = responseObj.getString("nombre_coup_dernier_echange");
                        String contestReussi = (String) responseObj.getJSONArray("constestation").get(0).toString();
                        String contestEchoue = (String) responseObj.getJSONArray("constestation").get(1).toString();

                        String state = "À venir";
                        JSONArray sets = responseObj.getJSONObject("pointage").getJSONArray("jeu");

                        String player1Set1 = "";
                        String player2Set1 = "";
                        String player1Set2 = "";
                        String player2Set2 = "";
                        String player1Set3 = "";
                        String player2Set3 = "";
                        if(!sets.isNull(0)){
                            player1Set1 = sets.getJSONArray(0).get(0).toString();
                            player2Set1 = sets.getJSONArray(0).get(1).toString();
                        }
                        if(!sets.isNull(1) ){
                            player1Set2 = sets.getJSONArray(1).get(0).toString();
                            player2Set2 = sets.getJSONArray(1).get(1).toString();
                        }
                        if(!sets.isNull(2) ){
                            player1Set3 = sets.getJSONArray(2).get(0).toString();
                            player2Set3 = sets.getJSONArray(2).get(1).toString();
                        }

                        /*resume = new MyListResume(terrain, tournament, date, tempPartie, player1Name, player1Rank, player1Age, player1Pays, player2Name, player2Rank, player2Age, player2Pays,
                                player1Set1, player1Set2, player1Set3, player2Set1, player2Set2, player2Set3, serveur, vitesse, nbCoup, contestReussi, contestEchoue);*/

                        ResumeMatch resumeMatch = new ResumeMatch();

                        TextView nomJoueur1 = Res.findViewById(R.id.player1Name);
                        nomJoueur1.setText(player1Name);

                        TextView nomJoueur2 = Res.findViewById(R.id.player2Name);
                        nomJoueur2.setText(player2Name);

                        TextView nom2Joueur1 = Res.findViewById(R.id.player1Name2);
                        nom2Joueur1.setText(player1Name);

                        TextView nom2Joueur2 = Res.findViewById(R.id.player2Name2);
                        nom2Joueur2.setText(player2Name);

                        TextView rangJoueur1 = Res.findViewById(R.id.player1Rank);
                        rangJoueur1.setText(player1Rank);

                        TextView rangJoueur2 = Res.findViewById(R.id.player2Rank);
                        rangJoueur2.setText(player2Rank);

                        TextView ageJoueur1 = Res.findViewById(R.id.player1Age);
                        ageJoueur1.setText(player1Age);

                        TextView ageJoueur2 = Res.findViewById(R.id.player2Age);
                        ageJoueur2.setText(player2Age);

                        TextView paysJoueur1 = Res.findViewById(R.id.player1Pays);
                        paysJoueur1.setText(player1Pays);

                        TextView paysJoueur2 = Res.findViewById(R.id.player2Pays);
                        paysJoueur2.setText(player2Pays);

                        TextView vitesseCoup = Res.findViewById(R.id.vitesse_coup);
                        vitesseCoup.setText(vitesse);

                        TextView terrainView = Res.findViewById(R.id.terrain1);
                        terrainView.setText(terrain);

                        TextView tempPartieView = Res.findViewById(R.id.tempspartie);
                        tempPartieView.setText(tempPartie);

                        TextView tournamentView = Res.findViewById(R.id.tournament);
                        tournamentView.setText(tournament);

                        TextView contRView = Res.findViewById(R.id.contR);
                        contRView.setText(contestReussi);

                        TextView contEView = Res.findViewById(R.id.contE);
                        contEView.setText(contestEchoue);

                        TextView nbcoupView = Res.findViewById(R.id.nbcoup);
                        nbcoupView.setText(nbCoup);

                        TextView dateView = Res.findViewById(R.id.date);
                        dateView.setText(date);

                        TextView set1p1 = Res.findViewById(R.id.player1Set1);
                        set1p1.setText(player1Set1);

                        TextView set1p2 = Res.findViewById(R.id.player2Set1);
                        set1p2.setText(player2Set1);

                        TextView set2p1 = Res.findViewById(R.id.player1Set2);
                        set2p1.setText(player1Set2);

                        TextView set2p2 = Res.findViewById(R.id.player2Set2);
                        set2p2.setText(player2Set2);

                        TextView set3p1 = Res.findViewById(R.id.player1Set3);
                        set3p1.setText(player1Set3);

                        TextView set3p2 = Res.findViewById(R.id.player2Set3);
                        set3p2.setText(player2Set3);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", "Error occurred ", error);
            }
        });

        queue.add(request);

    }


}
