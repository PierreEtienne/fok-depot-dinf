package com.example.tennis;

import android.content.Intent;
import android.view.View;

public class MyListResume {
    private String terrain;
    private String tournament;
    private String date;
    private String tempsPartie;
    private String player1Name;
    private String player1Rank;
    private String player1Age;
    private String player1Pays;
    private String player1Set1;
    private String player1Set2;
    private String player1Set3;
    private String player2Name;
    private String player2Rank;
    private String player2Age;
    private String player2Pays;
    private String player2Set1;
    private String player2Set2;
    private String player2Set3;
    private String tempsParti;
    private String serveur;
    private String vitesse;
    private String nbEchange;
    private String contR;
    private String contE;
    public MyListResume(String terrain, String tournament, String date, String tempsPartie, String player1Name, String player1Rank, String player1Age, String player1Pays, String player2Rank, String player2Name, String player2Age, String player2Pays, String player1Set1,
                      String player1Set2, String player1Set3, String player2Set1, String player2Set2, String player2Set3, String serveur, String vitesse, String nbEchange, String contR, String contE) {
        this.terrain = terrain;
        this.tournament = tournament;
        this.date = date;
        this.tempsParti = tempsPartie;
        this.player1Name = player1Name;
        this.player1Rank = player1Rank;
        this.player1Age = player1Age;
        this.player1Pays = player1Pays;
        this.player2Name = player2Name;
        this.player2Rank = player2Rank;
        this.player2Age = player2Age;
        this.player2Pays = player2Pays;
        this.player1Set1 = player1Set1;
        this.player1Set2 = player1Set2;
        this.player1Set3 = player1Set3;
        this.player2Set1 = player2Set1;
        this.player2Set2 = player2Set2;
        this.player2Set3 = player2Set3;
        this.serveur = serveur;
        this.vitesse = vitesse;
        this.nbEchange = nbEchange;
        this.contR = contR;
        this.contE = contE;
    }

    public MyListResume(){}

    public String getTerrain() {
        return terrain;
    }
    public void setTerrain(String terrain) {
        this.terrain = terrain;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getTempsPartie() {
        return tempsPartie;
    }
    public void setTempsPartie(String tempsPartie) {
        this.tempsPartie = tempsPartie;
    }
    public String getTournament() {
        return tournament;
    }
    public void setTournament(String tournament) {
        this.tournament = tournament;
    }
    public String getPlayer1Name() {
        return player1Name;
    }
    public void setPlayer1Name(String player1Name) {
        this.player1Name = player1Name;
    }
    public String getPlayer1Age() {
        return player1Age;
    }
    public void setPlayer1Age(String player1Age) {
        this.player1Age = player1Age;
    }
    public String getPlayer1Pays() {
        return player1Pays;
    }
    public void setPlayer1Pays(String player1Pays) {
        this.player1Pays = player1Pays;
    }
    public String getPlayer1Rank() {
        return player1Rank;
    }
    public void setPlayer1Rank(String player1Rank) {
        this.player1Rank = player1Rank;
    }
    public String getPlayer1Set1() {
        return player1Set1;
    }
    public void setPlayer1Set1(String player1Set1) {
        this.player1Set1 = player1Set1;
    }
    public String getPlayer1Set2() {
        return player1Set2;
    }
    public void setPlayer1Set2(String player1Set2) {
        this.player1Set2 = player1Set2;
    }
    public String getPlayer1Set3() {
        return player1Set3;
    }
    public void setPlayer1Set3(String player1Set3) {
        this.player1Set3 = player1Set3;
    }

    public String getPlayer2Name() {
        return player2Name;
    }
    public void setPlayer2Name(String player2Name) {
        this.player2Name = player2Name;
    }
    public String getPlayer2Rank() {
        return player2Rank;
    }
    public void setPlayer2Rank(String player2Rank) {
        this.player2Rank = player2Rank;
    }
    public String getPlayer2Age() {
        return player2Age;
    }
    public void setPlayer2Age(String player2Age) {
        this.player2Age = player2Age;
    }
    public String getPlayer2Pays() {
        return player2Pays;
    }
    public void setPlayer2Pays(String player2Pays) {
        this.player2Pays = player2Pays;
    }
    public String getPlayer2Set1() {
        return player2Set1;
    }
    public void setPlayer2Set1(String player2Set1) {
        this.player2Set1 = player2Set1;
    }
    public String getPlayer2Set2() {
        return player2Set2;
    }
    public void setPlayer2Set2(String player2Set2) {
        this.player2Set2 = player2Set2;
    }
    public String getPlayer2Set3() {
        return player2Set3;
    }
    public void setPlayer2Set3(String player2Set3) {
        this.player2Set3 = player2Set3;
    }
    public String getTempsParti() {
        return tempsParti;
    }
    public void setTempsParti(String tempsParti) {
        this.tempsParti = tempsParti;
    }
    public String getServeur() {
        return serveur;
    }
    public void setServeur(String serveur) {
        this.serveur = serveur;
    }
    public String getVitesse() {
        return vitesse;
    }
    public void setVitesse(String vitesse) {
        this.vitesse = vitesse;
    }
    public String getNbEchange() {
        return nbEchange;
    }
    public void setNbEchange(String nbEchange) {
        this.nbEchange = nbEchange;
    }
    public String getContR() {
        return contR;
    }
    public void setContR(String contR) {
        this.contR = contR;
    }
    public String getContE() {
        return contE;
    }
    public void setContE(String contE) {
        this.contE = contE;
    }


}



