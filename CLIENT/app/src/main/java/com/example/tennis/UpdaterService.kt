package com.example.tennis

import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.os.*
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

class UpdaterService : Service() {

    private var dataValues: ArrayList<MyListData> = ArrayList()
    private var serviceHandler: ServiceHandler? = null
    private var serviceLooper: Looper? = null
    private val TAG: String = "UpdaterService"
    private val notificationGroup: CustomObject = CustomObject("TennisGroupData", 479)

    private class CustomObject(val name: String, val prefix: Int)
    private class CustomNotificationObject(val index: Int, val notification: Notification)

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    private inner class ServiceHandler(looper: Looper): Handler(looper) {
        override fun handleMessage(msg: Message) {
            // query backend (use controller)
            Log.i(TAG, "from handleMessage")
            createNotification("Tennis", "Données mise à jour!")
        }
    }

    override fun onCreate() {
        Log.i(TAG, "from onCreate")
        HandlerThread("ServiceStartArguments", Process.THREAD_PRIORITY_BACKGROUND).apply {
            start()

            serviceLooper = looper
            serviceHandler = ServiceHandler(looper)
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.i(TAG, "from onStartCommand")
        Toast.makeText(this, "onStartCommand", Toast.LENGTH_LONG).show()

        Log.i(TAG, "from onStartCommand - action received: (${intent?.action})")
        when (intent?.action) {
            "init" -> {
                Log.i(TAG, "from onStartCommand - service started 'init'")
            }
            "refreshWithData" -> {
                val extraValue : ArrayList<MyListData> = intent.getParcelableArrayListExtra("dataList")!!
                Log.i(TAG, "$extraValue")
                updateNewValues(extraValue)
            }
            "mainRefresh" -> {
                /*serviceHandler?.obtainMessage()?.also { msg ->
                    msg.arg1 = startId
                    serviceHandler?.sendMessage(msg)
                }*/
            }
            else -> {}
        }

        return START_STICKY
    }

    private fun updateNewValues(arrayList: ArrayList<MyListData>) {
        // only compare the "state" of the match

        if (dataValues.size != arrayList.size) {
            dataValues = arrayList
        }

        val notifList: ArrayList<CustomNotificationObject> = ArrayList()
        var numberIndex = 0
        arrayList.forEach {
            val showNotification: Boolean = if (dataValues[numberIndex].state == arrayList[numberIndex].state) {
                dataValues[numberIndex].state == "En cours"
            } else {
                arrayList[numberIndex].state == "En cours" || dataValues[numberIndex].state == "En cours"
            }
            Log.i("UPDATERSERVICE", "SHOWNOTIFICATION $numberIndex ${arrayList[numberIndex].state} ${dataValues[numberIndex].state} $showNotification")

            if (showNotification) {
                var notificationContentText: String = if (arrayList[numberIndex].state == "Terminé") {
                    // WE HAVE A WINNER
                    "Victoire de ${findWinner(it)}!"
                } else {
                    "${it.player1Name} (${findWonSet(it, 1)}) vs ${it.player2Name} (${findWonSet(it, 2)})"
                }
                if (arrayList[numberIndex].state == "Terminé") {
                    val sharedPreferences = getSharedPreferences("SharedPreference", MODE_PRIVATE)
                    val myMoney = sharedPreferences.getInt("money", 500)
                    val betPlayer1 = sharedPreferences.getInt("betMatch"+numberIndex+"player1", 0)
                    val betPlayer2 = sharedPreferences.getInt("betMatch"+numberIndex+"player2", 0)

                    val myEdit = sharedPreferences.edit()
                    val winner = findWinner(it);
                    var newMoney = myMoney;

                    notificationContentText += if (winner == it.player1Name || winner == it.player2Name) {
                        "\nGain de ${if (winner == it.player1Name) betPlayer1 else betPlayer2}$!"
                    } else ""

                    newMoney += if (winner == it.player1Name || winner == it.player2Name) {
                        (if (winner == it.player1Name) betPlayer1 else betPlayer2) * 2
                    } else 0

                    myEdit.putInt("money", newMoney)
                    myEdit.apply()
                }

                notifList.add(
                    CustomNotificationObject(
                        numberIndex,
                        NotificationCompat.Builder(this, getString(R.string.channel_id))
                            .setSmallIcon(R.drawable.ic_launcher_foreground)
                            .setContentText(notificationContentText)
                            .setContentTitle("Match ${it.state}")
                            .setContentIntent(navigateToActivityClick(numberIndex))
                            .setGroup(notificationGroup.name)
                            .setAutoCancel(true)
                            .build()
                    )
                )
            }
            numberIndex += 1
        }

        val notifSummary: Notification = NotificationCompat.Builder(this, getString(R.string.channel_id))
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setGroup(notificationGroup.name)
            .setGroupSummary(true)
            .build()

        val instance = this
        NotificationManagerCompat.from(this).apply {
            notifList.forEach {
                notify(instance.notificationGroup.prefix + it.index, it.notification)
            }

            if (notifList.size > 0) {
                notify(instance.notificationGroup.prefix - 1, notifSummary)
            }
        }

        dataValues = arrayList
    }

    private fun findWonSet(matchData: MyListData, playerIndex: Int): String {
        val set1Winner = findSetWinner(convertStringToNumber(matchData.player1Set1), convertStringToNumber(matchData.player2Set1))
        val set2Winner = findSetWinner(convertStringToNumber(matchData.player1Set2), convertStringToNumber(matchData.player2Set2))
        val set3Winner = findSetWinner(convertStringToNumber(matchData.player1Set3), convertStringToNumber(matchData.player2Set3))

        var nbSet = 0
        if (set1Winner == playerIndex)
            nbSet += 1
        if (set2Winner == playerIndex)
            nbSet += 1
        if (set3Winner == playerIndex)
            nbSet += 1

        return nbSet.toString()
    }

    private fun findWinner(matchData: MyListData): String {
        val player1Win = findWonSet(matchData, 1) == "2"
        val player2Win = findWonSet(matchData, 2) == "2"

        return when {
            player1Win -> matchData.player1Name
            player2Win -> matchData.player2Name
            else -> "x"
        }
    }

    private fun findSetWinner(player1: Int, player2: Int): Int = when {
        player1 == 6 -> 1
        player2 == 6 -> 2
        else -> 0
    }

    override fun onDestroy() {
        Log.i(TAG, "from onDestroy")
        Toast.makeText(this, "onDestroy", Toast.LENGTH_LONG).show()
    }

    private fun convertStringToNumber(value: String): Int = when (value) {
        "" -> {
            0
        }
        else -> {
            value.toInt()
        }
    }

    private fun navigateToActivityClick(matchPosition: Int): PendingIntent {
        val parentIntent = Intent(this, MainActivity::class.java)
        val intentDetails = Intent(this, ResumeMatch::class.java).apply {
            this.putExtra("position", matchPosition)
        }

        return PendingIntent.getActivities(this, 123, arrayOf(parentIntent, intentDetails), PendingIntent.FLAG_UPDATE_CURRENT)
    }

    private fun createNotification(notificationTitle: String, notificationText: String) {
        /*val seeMatchIntent = Intent(this, ResumeMatch::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }*/

        val seeMatchsIntent = Intent(this, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }

        var theFlag = PendingIntent.FLAG_UPDATE_CURRENT
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            theFlag = theFlag or PendingIntent.FLAG_IMMUTABLE
        }

        val seeMatchPendingIntent = PendingIntent.getActivity(this,0,seeMatchsIntent,theFlag)
        val builder = NotificationCompat.Builder(this, getString(R.string.channel_id))
            .setContentIntent(seeMatchPendingIntent)
            .setContentText(notificationText)
            .setContentTitle(notificationTitle)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setAutoCancel(true)

        with(NotificationManagerCompat.from(this)) {
            notify(8,builder.build())
        }
    }
}