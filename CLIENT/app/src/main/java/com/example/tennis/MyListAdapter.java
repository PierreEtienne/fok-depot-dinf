package com.example.tennis;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.ArrayList;


public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.ViewHolder>{
    private ArrayList<MyListData> listdata;

    // RecyclerView recyclerView;
    public MyListAdapter(ArrayList<MyListData> listdata) {
        this.listdata = listdata;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final MyListData myListData = listdata.get(position);
        holder.stateView.setText(myListData.getState());
        holder.dateView.setText(myListData.getDate());
        holder.tournamentView.setText(myListData.getTournament());
        holder.player1NameView.setText(myListData.getPlayer1Name());
        holder.player1RankView.setText(myListData.getPlayer1Rank());
        holder.player1Set1View.setText(myListData.getPlayer1Set1());
        holder.player1Set2View.setText(myListData.getPlayer1Set2());
        holder.player1Set3View.setText(myListData.getPlayer1Set3());

        holder.player2NameView.setText(myListData.getPlayer2Name());
        holder.player2RankView.setText(myListData.getPlayer2Rank());
        holder.player2Set1View.setText(myListData.getPlayer2Set1());
        holder.player2Set2View.setText(myListData.getPlayer2Set2());
        holder.player2Set3View.setText(myListData.getPlayer2Set3());

        int position2 = position;

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ResumeMatch.class);
                intent.putExtra("position", position2);
                intent.putExtra("state", listdata.get(position2).getState());
                view.getContext().startActivity(intent);
            }

        });
    }


    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView stateView;
        public TextView dateView;
        public TextView tournamentView;
        public TextView player1NameView;
        public TextView player1RankView;
        public TextView player1Set1View;
        public TextView player1Set2View;
        public TextView player1Set3View;

        public TextView player2NameView;
        public TextView player2RankView;
        public TextView player2Set1View;
        public TextView player2Set2View;
        public TextView player2Set3View;
        public RelativeLayout relativeLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            this.stateView = (TextView) itemView.findViewById(R.id.state);
            this.dateView = (TextView) itemView.findViewById(R.id.date);
            this.tournamentView = (TextView) itemView.findViewById(R.id.tournament);
            this.player1NameView = (TextView) itemView.findViewById(R.id.player1Name);
            this.player1RankView = (TextView) itemView.findViewById(R.id.player1Rank);
            this.player1Set1View = (TextView) itemView.findViewById(R.id.player1Set1);
            this.player1Set2View = (TextView) itemView.findViewById(R.id.player1Set2);
            this.player1Set3View = (TextView) itemView.findViewById(R.id.player1Set3);
            this.player2NameView = (TextView) itemView.findViewById(R.id.player2Name);
            this.player2RankView = (TextView) itemView.findViewById(R.id.player2Rank);
            this.player2Set1View = (TextView) itemView.findViewById(R.id.player2Set1);
            this.player2Set2View = (TextView) itemView.findViewById(R.id.player2Set2);
            this.player2Set3View = (TextView) itemView.findViewById(R.id.player2Set3);
            relativeLayout = (RelativeLayout)itemView.findViewById(R.id.relativeLayout);
        }
    }
}