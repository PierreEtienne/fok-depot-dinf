package com.example.tennis

import android.os.Parcel
import android.os.Parcelable
import java.io.Serializable

data class MyListData(
    var state: String,
    var tournament: String,
    var date: String,
    var player1Name: String,
    var player1Rank: String,
    var player2Rank: String,
    var player2Name: String,
    var player1Set1: String,
    var player1Set2: String,
    var player1Set3: String,
    var player2Set1: String,
    var player2Set2: String,
    var player2Set3: String
) : Serializable, Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(state)
        parcel.writeString(tournament)
        parcel.writeString(date)
        parcel.writeString(player1Name)
        parcel.writeString(player1Rank)
        parcel.writeString(player2Rank)
        parcel.writeString(player2Name)
        parcel.writeString(player1Set1)
        parcel.writeString(player1Set2)
        parcel.writeString(player1Set3)
        parcel.writeString(player2Set1)
        parcel.writeString(player2Set2)
        parcel.writeString(player2Set3)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<MyListData> {
        override fun createFromParcel(parcel: Parcel): MyListData = MyListData(parcel)

        override fun newArray(size: Int): Array<MyListData?> = arrayOfNulls(size)
    }
}