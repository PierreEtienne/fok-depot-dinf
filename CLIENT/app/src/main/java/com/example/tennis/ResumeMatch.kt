package com.example.tennis

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import java.io.Serializable
import android.content.SharedPreferences




class ResumeMatch : AppCompatActivity() {
    lateinit var Resume: MyListResume;
    val context : Context = this
    var runLoop : Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resume_match)

        val intent: Intent = intent;
        val position: Int = intent.getIntExtra("position", 0);
        var controller = Controller.getInstance()
        controller.getDataPartie(this, position)

        val state: String? = intent.getStringExtra("state");

        betting(state, position);

        Thread(Runnable {
            refresh(controller)
        }).start()
    }

    override fun onPause() {
        super.onPause()
        runLoop = false
    }

    fun betting(state: String?, position: Int) {
        val sh = getSharedPreferences("SharedPreference", MODE_PRIVATE)
        val myMoney = sh.getInt("money", 500)
        val betPlayer1Original = sh.getInt("betMatch"+position+"player1", 0)
        val betPlayer2Original = sh.getInt("betMatch"+position+"player2", 0)

        val myMoneyText = findViewById<TextView>(R.id.myMoney)  as TextView
        myMoneyText.setText(myMoney.toString())

        val player1Input = findViewById<Button>(R.id.betPlayer1)  as Button
        val player2Input = findViewById<Button>(R.id.betPlayer2)  as Button
        val betValue = findViewById<EditText>(R.id.betValue)  as EditText
        val betPlayer1 = findViewById<TextView>(R.id.bet1SavedValue)  as TextView
        val betPlayer2 = findViewById<TextView>(R.id.bet2SavedValue)  as TextView
        val bettingMsg = findViewById<TextView>(R.id.bettingMsg)  as TextView

        betPlayer1.setText(betPlayer1Original.toString())
        betPlayer2.setText(betPlayer2Original.toString())

        player1Input.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View?) {
                val sharedPreferences = getSharedPreferences("SharedPreference", MODE_PRIVATE)
                var newMoney = sharedPreferences.getInt("money", 500)
                var betPlayer1New = sharedPreferences.getInt("betMatch"+position+"player1", 0)
                if(betValue.getText().toString() != "") {
                    val value = Integer.parseInt(betValue.getText().toString());
                    if(newMoney - value >= 0 && state != "Terminé"){
                        //set player 1 bet new value
                        val myEdit = sharedPreferences.edit()
                        betPlayer1New = betPlayer1New + value
                        myEdit.putInt("betMatch"+position+"player1", betPlayer1New)
                        betPlayer1.setText(betPlayer1New.toString());

                        //set the new cash left
                        newMoney = newMoney - value
                        myEdit.putInt("money", newMoney)
                        myEdit.apply()
                        myMoneyText.setText(newMoney.toString());
                    }
                    else{
                        bettingMsg.setText("Vous n'avez pas assez de fons ou la partie est terminé");
                    }
                }
                else{
                    bettingMsg.setText("Veuillez entrer une mise");
                }
            }
        })

        player2Input.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View?) {
                val sharedPreferences = getSharedPreferences("SharedPreference", MODE_PRIVATE)
                var newMoney = sharedPreferences.getInt("money", 500)
                var betPlayer2New = sharedPreferences.getInt("betMatch"+position+"player2", 0)
                if(betValue.getText().toString() != "") {
                    val value = Integer.parseInt(betValue.getText().toString());
                    if (newMoney - value >= 0 && state != "Terminé") {
                        //set player 1 bet new value
                        val myEdit = sharedPreferences.edit()
                        betPlayer2New = betPlayer2New + value
                        myEdit.putInt("betMatch"+position+"player2", betPlayer2New)
                        betPlayer2.setText(betPlayer2New.toString());

                        //set the new cash left
                        newMoney = newMoney - value
                        myEdit.putInt("money", newMoney)
                        myEdit.apply()
                        myMoneyText.setText(newMoney.toString());
                    } else {
                        bettingMsg.setText("Vous n'avez pas assez de fons ou la partie est terminé")
                    }
                }
                else{
                    bettingMsg.setText("Veuillez entrer une mise");
                }
            }
        })
    }

    private fun refresh(controller: Controller) {
        while(runLoop) {
            
            val intent: Intent = intent;
            val position: Int = intent.getIntExtra("position", 0);
            controller.getDataPartie(this, position)
            Thread.sleep(60_000)
        }
    }
}