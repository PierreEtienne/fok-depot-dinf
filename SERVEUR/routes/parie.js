const express = require("express");
const router = express.Router();
router.use(express.json());

const gen = require("../Generateur");
const configParie = require("../ConfigParie");

/* GET paries listing. */
router.get("/", function (req, res, next) {
  console.log("cash", configParie.getMontantJoueur());
  res.send(configParie.getMontantJoueur());
});

router.post("/", function (req, res, next) {
  res.send(
    configParie.ajouterParie(
      gen.liste_partie,
      req.body.partieId,
      req.body.joueurId,
      req.body.montant
    )
  );
});

module.exports = router;
