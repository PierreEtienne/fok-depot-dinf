class Pointage {
  constructor (parent) {
    this.manches = [0, 0];
    this.jeu = [[0, 0]];
    this.echange = [0, 0];
    this.etat = 'à venir';

    this.parent = parent;
  }

  ajouterPoint (joueur) {
    const mancheCourante = this.manches.reduce((a, b) => a + b, 0);
    this.etat = 'en cours';
    // incrementer l'echange
    this.echange[joueur] += 1;

    // si requis, incrementer le jeu
    if (this.echange[joueur] === 4) {
      this.echange = [0, 0];
      this.jeu[mancheCourante][joueur] += 1;
      this.parent.changerServeur();
    }

    // si requis, incrementer la manche
    if (this.jeu[mancheCourante][joueur] === 6) {
      this.manches[joueur] += 1;
      this.parent.nouvelleManche();
      if (this.manches[joueur] < 2) {
        this.jeu.push([0, 0]);
      }
    }

    // si le match est termine, le dire
    if (this.manches[joueur] === 2) {
      this.etat = 'terminé';
    }
  }

  terminerPremiereManche(){
    return this.manches[1] > 0 || this.manches[2] > 0;
  }

  toJSON () {
    return {
      'manches': this.manches,
      'jeu': this.jeu,
      'echange': this.echange,
      'etat': this.etat
    };
  }
}

module.exports = Pointage;
