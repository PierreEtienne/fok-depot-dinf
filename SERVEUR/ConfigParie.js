const fs = require("fs");
let montantJoueur = 500;
let banque = 0;

const getMontantJoueur = function () {
  return montantJoueur.toString();
};

const ajouterParie = function (listePartie, partieId, joueurId, montant) {
  var partie = listePartie[partieId];
  if (partie.pointage.terminerPremiereManche()) {
    return "Partie en cours impossible de parrier";
  }

  if (montant < montantJoueur) {
    montantJoueur -= montant;
    //SAUVEGARDER DANS FICHIERS CHAQUES TRANSACTIONS
    var stringToWrite =
      "Parie sur la partie" +
      partieId +
      " -------- Joueur" +
      joueurId +
      " -------- montant: " +
      montant +
      "\n";
    fs.appendFile("Historique.txt", stringToWrite, function (err) {
      if (err) {
        console.log(err);
      }
    });

    if (joueurId == 1) {
      partie.parieJoueur1 += montant;
    } else {
      partie.parieJoueur2 += montant;
    }
    return "ok";
  }
  return "Montant insuffisant";
};

const terminer = function (partie) {
  var montantPerdu;
  var montantGagne;

  if (partie.vainqueur() == 1) {
    montantPerdu = partie.getParieJoueur2();
    montantGagne = partie.getParieJoueur1();
  } else {
    montantPerdu = partie.getParieJoueur1();
    montantGagne = partie.getParieJoueur2();
  }

  //prendre le montant
  montantGagneApresFrais = montantGagne * 0.75;
  banque += montantGagne - montantGagneApresFrais + montantPerdu;

  montantJoueur += montantGagneApresFrais;

  console.log("---------------Banque-----------------");
  console.log(banque);
};

module.exports.ajouterParie = ajouterParie;
module.exports.terminer = terminer;
module.exports.getMontantJoueur = getMontantJoueur;

module.exports.montant_joueur = montantJoueur;
